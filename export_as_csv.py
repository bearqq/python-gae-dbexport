#!/usr/bin/env python 
# coding: utf-8

# Run from GAE remote API:
# 	{GAE Path}\remote_api_shell.py -s {YourAPPName}.appspot.com
# 	import export_as_csv

import sys
if sys.getdefaultencoding() != 'utf8':
	reload(sys)
	sys.setdefaultencoding( "utf-8" )

import csv
from db_model import dbls173b

import sqlite3
conn = sqlite3.connect('export.db')
c = conn.cursor()
c.execute("CREATE TABLE dbls173b (time integer, tid text, title text, bodytext text)")


def exportToCsv(query, csvFileName, delimiter):
	with open(csvFileName, 'wb') as csvFile:
		csvWriter = csv.writer(csvFile, delimiter=delimiter, quotechar='|', quoting=csv.QUOTE_MINIMAL)
		writeHeader(csvWriter)

		rowsPerQuery = 1000
		totalRowsSaved = 0
		cursor = None
		areMoreRows = True

		while areMoreRows:
			if cursor is not None:
				query.with_cursor(cursor)
			items = query.fetch(rowsPerQuery)
			cursor = query.cursor()

			currentRows =0
			for item in items:
				saveItem(csvWriter, item)
				currentRows += 1

			totalRowsSaved += currentRows
			areMoreRows = currentRows >= rowsPerQuery
			print 'Saved ' + str(totalRowsSaved) + ' rows'

		print 'Finished saving all rows.'
		conn.commit()
		conn.close()
		

def writeHeader(csvWriter):
	csvWriter.writerow(['time', 'tid', 'title', 'bodytext']) #Output csv header

def saveItem(csvWriter, item):
	csvWriter.writerow([item.time,item.tid,item.title,item.bodytext])
	c.execute("INSERT INTO dbls173b VALUES (?,?,?,?)",(item.time,item.tid,item.title,item.bodytext)) # Save items in preferred format


query = dbls173b.gql("ORDER BY time") #Query for items
exportToCsv(query, 'myExport.csv', ",")
